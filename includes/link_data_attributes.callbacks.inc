<?php
/**
 * @file
 * Callback functions to add a custom HTML 5 data attribute inside link fields.
 */

/**
 * Process field data.
 */
function link_data_attributes_process($element) {
  if (!isset($element['#entity_type'])) {
    return $element;
  }

  $instance = field_info_instance($element['#entity_type'], $element['#field_name'], $element['#bundle']);

  if (!empty($instance['settings']['dataattribute']['enable'])) {
    $element['attributes']['dataattribute'] = [
      '#type' => 'textfield',
      '#title' => check_plain($instance['settings']['dataattribute']['title']),
      '#description' => check_plain($instance['settings']['dataattribute']['description']),
      '#prefix' => check_plain($instance['settings']['dataattribute']['prefix']),
      '#suffix' => check_plain($instance['settings']['dataattribute']['suffix']),
      '#maxlength' => check_plain($instance['settings']['dataattribute']['maxlength']),
      '#default_value' => isset($element['#default_value']['attributes']['dataattribute']) ? check_plain($element['#default_value']['attributes']['dataattribute']) : '',
    ];
  }

  if (isset($instance['settings']['dataattribute']) && $instance['settings']['dataattribute']['display']) {
    $element['attributes']['dataname'] = [
      '#type' => 'hidden',
      '#default_value' => $instance['settings']['dataattribute']['dataname'],
    ];
  }

  return $element;
}
