Module description
------------------
The Link Data Attribute module provides a standard custom HTML 5 data-* 
attribute for links fields. With this module links can be easily added with a 
custom value for each link (also when a multi valued link field is used). When
 editing a content with a Link field this module enables the ability to, on 
site building (editing a content type for example), to customize what will be 
displayed on content editing for this new field. The custom data added to each 
link will be accessible and may be used programmatically on any hook for that 
link field. It also enables, on site building, the possibility to 
automatically add a HTML 5 data-* attribute to that link (being * configured 
on site building as well), displaying the value inserted on content editing.

Requirements / Dependencies
---------------------------
1. Drupal 7: Link module.
2. Drupal 8: None, since Link module is in core now. (It was not ported yet!)

Installation
------------
1. Drop the entire module folder into your 'sites/all/modules/contrib/' folder
2. Enable the module from the Administration area modules page (admin/modules)
3. Create or Edit a content-type and add a new field of type link
4. Enable and configure LINK DATA ATTRIBUTE section
5. Repeat item #4 for each new Link field added which you want this enabled

Configuration
-------------
Configuration is only slightly more complicated than a link field. You may 
configure what it will be displayed when editing a content with this field 
(Including Title, Description, Prefix, Suffix and Max length) and also if it 
will be added as a custom HTML 5 data-* attribute (being * configured there as 
well). When editing a content for a configured link it will appear a field 
with all the configuration that were just set up and ask for the custom data 
you want to add for it.

Example
-------
If you were to create a link field with HTML 5 Data Attribute enable and 
displayed, the default display of the link would be:  
<a href="URL" data-[attribute]="[value]">Title</a>, where items between [] 
characters would be customized based on the user input (on site building 
and content edition).

Theming and Output
------------------
If HTML 5 data-* attribute is configured to be displayed, it will be 
automatically added on render function with the configuration seted up on site 
building and values inserted on content edition.
